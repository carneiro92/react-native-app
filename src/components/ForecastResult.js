import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { View, Text, Image, StyleSheet } from "react-native";

class ForecastResult extends Component {
  render() {
    if (this.props.forecast.location.name != "") {
      const image = { uri: this.props.forecast.current.weather_icons[0] };
      return (

        <View style={styles.ResultView}>
          <View style={styles.ResultViewTop}>
            <View style={styles.ResultViewTopLeft}>
              <Image alt="Weather" source={image} style={styles.image} />
            </View>
            <View style={styles.ResultViewTopRight}>
              <Text style={styles.weatherDesc}>
                {this.props.forecast.current.weather_descriptions}
              </Text>
            </View>
          </View>
          <View style={styles.ResultViewTop}>
            <View style={styles.ResultViewMiddleLeft}>
              <Text style={styles.weatherTemp}>
                {this.props.forecast.current.temperature} ° C
              </Text>
            </View>
            <View style={styles.ResultViewMiddleRight}>
              <Text style={styles.weatherText}>
                Wind Speed: {this.props.forecast.current.wind_speed}
              </Text>
              <Text style={styles.weatherText}>
                Humidity: {this.props.forecast.current.humidity}
              </Text>
            </View>
          </View>
        </View>
      );
    } else {
      return <Text style={styles.searchText}>Cherchez une Localization</Text>;
    }
  }
}
const mapStateToProps = (state) => {
  return {
    forecast: state.weatherReducer.forecast,
  };
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
const styles = StyleSheet.create({
  ResultView: {
    width: "90%",
    height: "100%",
    flexDirection: "column",
    backgroundColor: "rgba(255, 255, 255, 1)",
    borderRadius: 12,
    opacity: 0.9,
    margin:"5%"
  },
  ResultViewTop: {
    flex: 1,
    flexDirection: "row",
    left: 10,
    width: "95%",
    borderRadius: 12,
    opacity: 0.8,
  },
  ResultViewTopLeft: {
    flex: 0.4,
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  ResultViewTopRight: {
    flex: 0.6,
    justifyContent: "space-evenly",
    alignItems: "stretch",
  },
  ResultViewMiddleLeft: {
    flex: 0.6,
    alignItems: "center",
  },
  ResultViewMiddleRight: {
    flex: 0.4,
    alignItems: "center",
  },
  weatherTemp: {
    textAlign: "center",
    color: "rgba(64, 68, 145, 1)",
    fontSize: 80,
    fontWeight: "bold",
    opacity: 1,
    letterSpacing: 0,
  },
  weatherDesc: {
    textAlign: "left",
    color: "rgba(64, 68, 145, 1)",
    fontSize: 50,
    fontWeight: "bold",
    opacity: 1,
    letterSpacing: 0,
  },
  weatherText: {
    marginTop:10,
    fontSize: 20,
    fontWeight: "800",
    color: "rgba(112, 112, 112, 1)",
  },
  image: {
    width: 82,
    height: 91,
    opacity: 0.7,
  },
  searchText: {
    top: 150,
    fontStyle: "italic",
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 50,
    color: "#FFFFFF",
    justifyContent: "center",
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(ForecastResult);
