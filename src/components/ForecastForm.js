import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { updateInput, updateForecastWithFetched } from "../actions/weather";
import { StyleSheet, TextInput, View, Button, Text, } from "react-native";


class ForecastForm extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      city: "",
    };
  }

  updateCity = (value) => {
    this.setState({
      city: value,
    });
  };

  fetchForecast = () => {
    this.props.updateForecastWithFetched(this.state.city);
  };
  
  render() {
    
    var load;
    if (!this.props.loader) {
      load = <Button title="Search" color='#007AFF' onPress={this.fetchForecast} />;
    } else {
      load = <Text>Loading</Text>;
    }
    return (

      <View style={styles.container}>
        <TextInput
          clearTextOnFocus= {true}
          style={styles.text}
          value={this.state.city}
          onChangeText={this.updateCity}
          placeholder="Rechercher une ville"
          onSubmitEditing={this.fetchForecast}
          
        ></TextInput>
        {load}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerM: {
    flex: 1,
  },
  container: {
    top: '25%',
    flex: 1,
    flexDirection: "column",
  },
  image: {
    flex: 1,
    resizeMode: "center",
    justifyContent: "center",
  },
  text: {
    textAlign: "center",
    fontSize:30,
    left: 20,
    height: 50,
    width: '90%',
    backgroundColor: "white",
    opacity: 0.9,
    borderRadius: 12,
  },
  button: {
    backgroundColor: '#00aeef',
    borderColor: 'red',
    borderWidth: 5,
    borderRadius: 15       
 }
});
const mapStateToProps = (state) => {
  return {
    city: state.weatherReducer.input,
    loader: state.weatherReducer.loader,
  };
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      updateInput,
      updateForecastWithFetched,
    },
    dispatch
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(ForecastForm);
