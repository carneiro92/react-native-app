import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { View, Text, StyleSheet } from "react-native";

class ForecastTitle extends Component {
  render() {
    const date = Date(this.props.forecast.location.localtime);
    const treatedDate = date.replace("GMT+0100 (CET)", "");
    return (
      <View style={styles.titleContainer}>
        <Text style={styles.title}>
          {this.props.forecast.location.name}
        </Text>
        <Text style={styles.date}>{treatedDate}</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  titleContainer: {
    top:39,

  },
  date: {
    fontStyle:'italic',
    fontWeight: 'bold',
    textAlign:'center',
    fontSize:20,
    color: '#FFFFFF',
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  title: {
    fontStyle:'italic',
    fontWeight: 'bold',
    textAlign:'center',
    fontSize:50,
    color: '#FFFFFF',
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});
const mapStateToProps = (state) => {
  return {
    forecast: state.weatherReducer.forecast,
  };
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(ForecastTitle);
