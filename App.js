import React from "react";
import { StyleSheet, Text, View, ImageBackground } from "react-native";
import { Provider } from "react-redux";
import Forecast from "./src/components/Forecast";
import store from "./store";
import image from "./assets/splash.png"
export default function App() {
  return (
    <Provider store={store}>
      <ImageBackground source={image} style={styles.image}>
        <Forecast/>
      </ImageBackground>
    </Provider>
  );
}

const styles = StyleSheet.create({
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});
