import thunk from 'redux-thunk';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import weatherReducer from './src/reducers/weatherReducer';

const middleware = [thunk];

const store = createStore(
  combineReducers({ weatherReducer }),
  applyMiddleware(...middleware)
);
export default store;
